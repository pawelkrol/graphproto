CHANGES
=======

0.03-SNAPSHOT (2024-10-10)
--------------------------

* Utilised [Scala-ConfigManager] library to store application settings

0.02 (2023-05-26)
-----------------

* Bugfixed an auxiliary Bresenham's line algorithm implementation to also include a missing initial pixel of line points computation (previously it was incorrectly ignored)
* Added _Rotating Cube_ example to the list of widgets bundled with the default application

0.01 (2017-01-30)
-----------------

* Initial version (provides a starting point and a ready-to-use rendering engine for implementing miscellaneous graphic algorithms with two predefined example animations, a _Midpoint Circle_ and a _Satan Cross,_ as well as two additional step-by-step tutorials, a minimal _Hello World_ and a bit more complex _Rotating Cube)_


[Scala-ConfigManager]: https://github.com/pawelkrol/Scala-ConfigManager
