Rotating Cube
=============

This tutorial assumes you are already familiar with a [Hello World](https://bitbucket.org/pawelkrol/graphproto/src/master/doc/HELLO.md) example, leaving out some basic details of `GraphProto` interfaces, and focusing on what is actually important, i.e. implementing an actual rotating cube algorithm.

![Rotating Cube Screenshot](https://bitbucket.org/pawelkrol/graphproto/raw/master/doc/rotatingcube-screenshot.png)

Let us begin by reading a complete listing of each source code file and discuss them all in details afterwards.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Widget/RotatingCubeFrame.scala
--------------------------------------------------------------------------------

`RotatingCubeFrame` class defines a single animation frame based off three rotation angles through which cube rotation is executed.

    package org.bitbucket.pawelkrol.GraphProto
    package Widget

    import java.awt.{ Color, Graphics2D }
    import java.awt.image.BufferedImage
    import java.awt.image.BufferedImage.TYPE_INT_RGB

    import java.lang.Math.{ cos, sin }

    import Algorithm.{ Polygon, Shape, Vertex }

    class RotatingCubeFrame(width: Int, height: Int, angleX: Double, angleY: Double, angleZ: Double) extends Frame {

      private val background = new Color(0xee, 0xe8, 0xd5)

      private val color = new Color(0x07, 0x36, 0x42)

      private val observer = Vertex(0, 0, -1024)

      private val distance = height.toDouble / 0.75

      private val center = Vertex(width / 2, height / 2, 0)

      private val rotatedShape = RotatingCubeFrame.shape.rotate(Vertex(angleX, 0, 0)).rotate(Vertex(0, angleY, 0)).rotate(Vertex(0, 0, angleZ)).translate(-observer)

      private val visiblePolygons = rotatedShape.visiblePolygons(observer)

      private val castedPolygons = visiblePolygons.map(_.perspectiveCast(distance))

      private val translatedPolygons = castedPolygons.map(_.translate(center))

      private def drawPolygons(g2d: Graphics2D): Unit = {
        translatedPolygons.map(_.draw(g2d))
      }

      private val cube = {

        val img = new BufferedImage(width, height, TYPE_INT_RGB)

        val g2d = img.createGraphics
        g2d.setColor(background)
        g2d.fillRect(0, 0, width, height)
        g2d.setColor(color)
        drawPolygons(g2d)
        g2d.dispose

        img
      }

      lazy val canvas = AWTCanvas(cube)
    }

    object RotatingCubeFrame {

      private val vertices = List(
        (-200.0, 200.0, -200.0),
        (200.0, 200.0, -200.0),
        (200.0, -200.0, -200.0),
        (-200.0, -200.0, -200.0),
        (-200.0, 200.0, 200.0),
        (200.0, 200.0, 200.0),
        (200.0, -200.0, 200.0),
        (-200.0, -200.0, 200.0)
      ).map(Vertex(_))

      private val colors = Seq(
        new Color(0xb5, 0x89, 0x00),
        new Color(0xcb, 0x4b, 0x16),
        new Color(0x6c, 0x71, 0xc4),
        new Color(0x26, 0x8b, 0xd2),
        new Color(0x2a, 0xa1, 0x98),
        new Color(0x85, 0x99, 0x00)
      )

      private val shape = Shape(Seq(
        Seq(vertices(0), vertices(1), vertices(2), vertices(3)),
        Seq(vertices(4), vertices(7), vertices(6), vertices(5)),
        Seq(vertices(4), vertices(0), vertices(3), vertices(7)),
        Seq(vertices(1), vertices(5), vertices(6), vertices(2)),
        Seq(vertices(0), vertices(4), vertices(5), vertices(1)),
        Seq(vertices(3), vertices(2), vertices(6), vertices(7))
      ).zip(colors).map({ case (vertices, color) => Polygon(vertices, Some(color)) }))

      def apply(width: Int, height: Int, angleX: Double, angleY: Double, angleZ: Double) =
        new RotatingCubeFrame(width, height, angleX, angleY, angleZ)
    }

Let's have a look at the `RotatingCubeFrame` object first. This is were a physical shape of a rotating cube is defined. `vertices` declares a list of vertices forming a cubic shape. They are uniformly distributed around the origin of a coordinate system, which appears precisely in the middle of them. `colors` is a list of colors sequentially applied to all constructed cube faces for a better visual effect. `shape` yields a definition of a `Shape` object built from prior defined vertices. A sequence of vertices forms a `Polygon` (which in this case is always a square in a three-dimensional space). Every polygon is zipped with a prior defined color, so that all cube faces may be rendered using a different hue. Initialization of a new `RotatingCubeFrame` instance is possible via an `apply` method that accepts exactly the same argument list as a primary constructor of a `RotatingCubeFrame` class. This code is straight-forward and pretty self-explanatory, so we can move on at once to a `RotatingCubeFrame` class definition.

First a couple of constants get defined: background and outline colors are `java.awt.Color` objects, and an observer's position is an instance of GraphProto's `Vertex`, which is a pretty universal class here that may serve several different purposes, i.a. being a polygon's vertex, an individual point, or just a vector (as `observer` is treated in this particular case). Observer's `z` location is set to an arbitrarily large value, with `x` and `y` coordinates equal to the origin of a coordinate system. The `z` value of an observer's vector is pointing towards the eyes of a viewer as an object defined via `shape` variable is pushed back by the same number of pixels once all rotation transformations have been applied. Last but not least a `center` value describes a physical center of an application window, this is a number of pixels that casted shape needs to be moved by in order to appear exactly in the middle of an application window.

Once these few auxiliary values are defined, all the necessary computations may be performed. They are executed in the following order:

1. Rotation in 3D according to initialization arguments (with each plane being rotated separately).
2. Translation in 3D (pushing a cube back relative to observer's position).
3. Selection of polygons visible to an observer (hiding invisible cube faces).
4. Perspective projection of a 3D model to a two-dimensional plane (a computer's screen).
5. Translation in 2D (centering a rendered image in the middle of a program's window).

Such a defined cube object is rendered onto an image canvas in a `drawPolygons` method. Image canvas is an instance of `java.awt.image.Image` that is expected to initialize GraphProto's `AWTCanvas` instance stored in a `canvas` property of a `RotatingCubeFrame` class that conforms to a `Frame` interface it extends.

All heavy math calculations are well-hidden from a programmer, however it is not impossible for you to sneak into implementation details of `Polygon`, `Shape`, and `Vertex` classes in order to figure out how the magic behind rotation, translation and perspective casting actually works.

This concludes how a single animation frame of a rotating cube is created.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Widget/RotatingCube.scala
---------------------------------------------------------------------------

`RotatingCube` model is set up with a definition of an algorithm that computes all animation frames of a cube prototype, precisely its changing rotation angles.

    package org.bitbucket.pawelkrol.GraphProto
    package Widget

    import java.lang.Math.PI

    class RotatingCube extends Model {

      lazy val dimension = HiresBitmap()

      protected def algorithm(frame: Int) = {
        val angleX = 4 * PI * frame / numFrames
        val angleY = 2 * PI * frame / numFrames
        val angleZ = -2 * PI * frame / numFrames

        RotatingCubeFrame(dimension.width, dimension.height, angleX, angleY, angleZ)
      }
    }

    object RotatingCube {

      def apply() = new RotatingCube
    }

Initialization of a new `RotatingCube` instance is possible via an `apply` method. `dimension` value is defined as a hires bitmap, which basically means that each frame will be rendered onto a 320x200 pixels large canvas, and later resized according to a dimension of an application window that in turn is defined via an instance of a `Renderer` class that in turn is declared in a `Panel` class. The second part of this statement is probably less important, since these are just a few internal details of GraphProto's implementation. What you definitely want to know (and therefore forward it to each new instance of `RotatingCubeFrame`) is an original canvas size on which calculated graphic data may be rendered to.

An `algorithm` method is implicitly called once animation frames of a `RotatingCube` are prerendered. This happens during an initialization phase of an executed program. We use it to declare three rotation angles, around `x`, `y`, and `z` axes respectively. Their calculation is based off the current frame number to ensure a smooth transition between edge frames and to enable looping of a whole animation. Once rotation angles have been computed, the very last thing to do is the initialization of an instance of a `RotatingCubeFrame` class and returning it as a result of an `algorithm` method invocation.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Application.scala
-------------------------------------------------------------------

Just like with a [Hello World](https://bitbucket.org/pawelkrol/graphproto/src/master/doc/HELLO.md) example, the main goal of writing this code and using _GraphProto_ library at all is to watch the final result of your work on a computer screen. Conforming to the rules of predefined _GraphProto_'s interfaces makes this step super easy. It is sufficient to instantiate an object of `RotatingCube` class, assign a label to it and voilà, an animated rotating cube becomes visible as soon as you pick a respective item from the _Objects_ menu just after starting an example program.

Here is a diff demonstrating what kind of changes need to be applied to a discussed file:

     val widgets = List(
       ("Midpoint Circle", Circle()),
    -  ("Satan Cross", SatanCross())
    +  ("Satan Cross", SatanCross()),
    +  ("Rotating Cube", RotatingCube())
     )

This notation requires an import of `RotatingCube` name from `Widget` package if you want to use it without qualification:

    -import Widget.{ Circle, SatanCross }
    +import Widget.{ Circle, RotatingCube, SatanCross }

What you are going to see is the exactly same picture as a cube presented at the very top of this page.
