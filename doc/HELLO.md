Hello World
===========

This document describes a process of setting up a minimal _Hello World_-like application using features provided by `GraphProto` prototyping library. Upon completion of this tutorial you will see a _Hello World_ text bouncing happily within an application window.

![Hello World Screenshot](https://bitbucket.org/pawelkrol/graphproto/raw/master/doc/helloworld-screenshot.png)

Assuming you have cloned a repository, open a text editor of your choice and edit the following three files.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Widget/HelloWorldFrame.scala
------------------------------------------------------------------------------

First you need to create a file describing a single animation frame. In order to take advantage of a couple of auxiliary routines already provided by `GraphProto` you should conform to a `Frame` interface. You do this by extending your own frame class with a `Frame` trait, which expects you to implement just a `canvas` method. You can begin with a sketch of your class without a definition of an actual `canvas` routine.

    import org.bitbucket.pawelkrol.GraphProto.Frame

    class HelloWorldFrame extends Frame {

      val canvas = ...
    }

The next question you need to answer is what kind of variables are expected to change between subsequent frames of your animation procedure. These variables will have to be provided externally via a constructor of your class. Let us suppose that a _Hello World_ text is going to be rendered using a static text box, however its position on a screen is going to successively change. If that is the case you probably want to have your `Frame`-derived class to be initialised with coordinates of a new position, and use this position as a starting point to render a _Hello World_ text. Of course if your _Hello World_ application had not required any animation frames at all, you could have left out this whole reasoning and either render a single frame each and every time, skipping your frame's parameterisation entirely, or even think about further simplifications that do not involve designing a `Frame`-derived class at all. For now, however, focus on moving your rendered text around a screen, and for this you need to initialize `HelloWorldFrame` with `x` and `y` integers.

    import org.bitbucket.pawelkrol.GraphProto.Frame

    class HelloWorldFrame(x: Int, y: Int) extends Frame {

      val canvas = ...
    }

What shall `canvas` return? It shall return an instance of `Canvas` interface. `Canvas` is another trait defined by `GraphProto`. You are free to derive your own implementation, but you may as well use one of a few predefined `Canvas` classes already providede by `GraphProto`. `AWTCanvas` is one of them. It expects a single argument `image` upon initialization, which is an instance of `java.awt.Image` class. Let us construct one and use it to initialize our `AWTCanvas` instance. There is one more catch you are likely to notice at this point. When constructing a new image object you have to know its size. `GraphProto` comes to the rescue and exposes two essential properties of a screen via `Renderer` object: `defaultWidth` and `defaultHeight`. This same challenge may be approached in a different way when using a different `Canvas`-derived class, for now we focus on how to use `AWTCanvas`.

    import java.awt.image.BufferedImage
    import java.awt.image.BufferedImage.TYPE_INT_RGB

    import org.bitbucket.pawelkrol.GraphProto.Frame

    class HelloWorldFrame(x: Int, y: Int) extends Frame {

      val canvas = {
        val width = Renderer.defaultWidth
        val height = Renderer.defaultHeight

        new BufferedImage(width, height, TYPE_INT_RGB)
      }
    }

You are almost done setting up a single animation frame, what is left is placing a _Hello World_ text into the image at a desired target position. This part of code is fortunately completely independent from `GraphProto` features, essentially you can do whatever you want with your image. Below is the final overview of a complete `HelloWorldFrame` you will use to render a single animation frame of your _Hello World_ program.

    package org.bitbucket.pawelkrol.GraphProto
    package Widget

    import java.awt.{ Color, Font }
    import java.awt.image.BufferedImage
    import java.awt.image.BufferedImage.TYPE_INT_RGB

    class HelloWorldFrame(x: Int, y: Int) extends Frame {

      private val background = new Color(0xee, 0xe8, 0xd5)

      private val color = new Color(0x65, 0x7b, 0x83)

      private val image = {

        val width = Renderer.defaultWidth
        val height = Renderer.defaultHeight

        val img = new BufferedImage(width, height, TYPE_INT_RGB)
        val font = new Font("Sans-Serif", Font.BOLD, 64)

        val g2d = img.createGraphics
        g2d.setColor(background)
        g2d.fillRect(0, 0, width, height)
        g2d.setColor(color)
        g2d.setFont(font)
        g2d.drawString("Hello World", x, y)
        g2d.dispose

        img
      }

      val canvas = AWTCanvas(image)
    }

    object HelloWorldFrame {

      def apply(x: Int, y: Int) = new HelloWorldFrame(x, y)
    }

You may want to investigate other available `Canvas` classes to see what other methods of placing pixels into a target image you have or even derive your own techniques.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Widget/HelloWorld.scala
-------------------------------------------------------------------------

Now is the time to design our main `HelloWorld` class. This class will be responsible for rendering all animation frames. Provided `HelloWorldFrame` takes care of most rendering requirements, the only thing left to `HelloWorld` class is an implementation of an algorithm that moves _Hello World_ text around a screen and initialises `HelloWorldFrame` instances with proper _x_ and _y_ coordinates for each displayed animation frame.

Conforming to `GraphProto` interfaces gives you an access to some ready-to-use methods and will later allow you to simply plug it into a UI. The interface we are particularly interested in right now is named `Model`. `Model` expects you to provide a definition of two methods: `dimension` and `algorithm`.

Now, `dimension` requires a little bit more detailed explanation, because `dimension` that is defined in your `Model` is not the same `dimension` that was earlier picked up from a `Renderer` object. It is a mere coincidence that in our _Hello World_ application logical and rendered image dimensions match, however it does not necessarily need to always be true as is demonstrated by two predefined example algorithms, _Midpoint Circle_ and _Satan Cross._ And it is especially visible in the latter one. _Satan Cross_ operates on a 40x25 pixels large canvas layer. This canvas is later upscaled by a `Renderer` in order to fill out an entire window screen with magnified pixels. In our _Hello World_ application we are assigning an instance of a predefined `DefaultWindow` class to describe `dimension` of our picture, which happens to be of an exactly same size as default sizes of `Renderer` object, however it could have been something else. One question that arises here is how a `Frame` object would learn about the size of your model. Well, it can learn about it in no other way than by passing this information via an argument to its constructor, thus extending the list of your `Frame`-derived class properties with a logical dimension of a target image.

What about `algorithm`? A situation here is much simpler. It is a method expected to return a `Frame` object, given a frame index as its sole argument. `Model` trait gives you access to `numFrames` property, so that you can learn which frame in order would be rendered. Because `HelloWorldFrame` expects to be initialised using two coordinate attributes, you may use a little bit of math to create a bouncing path for your text.

    import java.lang.Math.{ PI, sin }

    val x = 270 + 240 * sin(4 * PI * frame / numFrames)
    val y = 320 + 120 * sin(6 * 2 * PI * frame / numFrames)

These hardcoded integers could have been derived in a smarter way, however it is not the point of this tutorial to discuss possible math recipes in details, but rather to help you understand how to use `GraphProto` library in order to help you accomplish your prototyping goals. Below is the final overview of a `HelloWorld` class, which implements an above computation and defines an algorithm returning a new `HelloWorldFrame` instance for each animation frame.

    package org.bitbucket.pawelkrol.GraphProto
    package Widget

    import java.lang.Math.{ PI, sin }

    class HelloWorld extends Model {

      lazy val dimension = DefaultWindow()

      protected def algorithm(frame: Int) = {

        val x = 270 + 240 * sin(4 * PI * frame / numFrames)
        val y = 320 + 120 * sin(6 * 2 * PI * frame / numFrames)

        HelloWorldFrame(x.toInt, y.toInt)
      }
    }

    object HelloWorld {

      def apply() = new HelloWorld
    }

You are encouraged to experiment with this code to design more and more complex algorithms. Try starting small, maybe by modifying the path around which your _Hello World_ text travels. In the next step you could think about zooming a text in and out, which could be achieved through modification of a font size in `HelloWorldFrame` that needs to be passed as an argument to its initialiser and defined within your implementation of an `algorithm` method.

src/main/scala/org/bitbucket/pawelkrol/GraphProto/Application.scala
-------------------------------------------------------------------

The very last thing left to do is bringing everything together. You may conclude your little project by simply referencing a newly created animation widget inside a main application window. There is a list of `widgets` which can be simply extended with as many objects as you like. Here is a diff adding a `HelloWorld` instance on a third position in a list:

     val widgets = List(
       ("Midpoint Circle", Circle()),
    -  ("Satan Cross", SatanCross())
    +  ("Satan Cross", SatanCross()),
    +  ("Hello World", HelloWorld())
     )

Do not forget to import `HelloWorld` name from `Widget` package to make it available without qualification:

    -import Widget.{ Circle, SatanCross }
    +import Widget.{ Circle, HelloWorld, SatanCross }

And that is all! Go ahead and `run` it!

If you are looking for something more impressive than just a simple _Hello World_ text moving around a screen, check out [Rotating Cube](https://bitbucket.org/pawelkrol/graphproto/src/master/doc/CUBE.md) tutorial that offers a very detailed example on definining objects in a 3D space, transforming them and eventually projecting them on a screen using perspective casting.
