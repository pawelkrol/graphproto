GraphProto
==========

`GraphProto` is a simple ready-to-use prototyping library originally designed to support visualisation of miscellaneous graphic algorithm implementations. You may either derive your own components based on a few provided examples or simply extend an existing application by adding the new ones. The main reason for using `GraphProto` is to let you concentrate on an actual algorithm implementing your animation frame(s), leaving out all unpleasant details and specifics of a rendering engine to a library, allowing you to focus on what is really important in graphics programming. This program is entirely written in [Scala](http://scala-lang.org/) and built on top of a popular GUI framework [Scala Swing](http://www.scala-lang.org/api/current/scala-swing/scala/swing/index.html).

Version
-------

Version 0.03-SNAPSHOT (2024-10-10)

Installation
------------

Add the following automatic export to your `~/.bash_profile`:

    export _JAVA_OPTIONS="-Xms1024m -Xmx2G -Xss256m"

In order to build and run an application JAR type the following:

    $ git clone git://bitbucket.com/pawelkrol/GraphProto.git
    $ cd GraphProto
    $ sbt clean update compile package proguard:proguard run
    $ java -Dfile.encoding=UTF8 -jar target/scala-3.5.1/proguard/graphproto-0.03-SNAPSHOT.jar

Guidelines
----------

There are only a few steps you need to perform in order to setup a new widget, and they are all listed in this section. _A widget_ and _an object_ terms are both used interchangeably here, and their sole purpose is to represent a dedicated canvas layer that a library uses to render all pixels illustrating your specific graphic algorithm.

![Midpoint Circle Screenshot](https://bitbucket.org/pawelkrol/graphproto/raw/master/doc/midpointcircle-screenshot.png) ![Satan Cross Screenshot](https://bitbucket.org/pawelkrol/graphproto/raw/master/doc/satancross-screenshot.png)

We are going to split this section into two parts. The first one describes the simplest example of a single class the may be used to render a solitary picture that is steadily displayed on a computer screen while the code still conforms to _GraphProto_'s interfaces. You learn the true power of a framework once you add more dynamics to your rendering algorithm and set it up with animation frames. Nothing stops you either from reworking an entire internal structure of all available classes and derive your own scheme of things. This however goes way beyond a scope of this tutorial. Go ahead and explore the code base, and feel free to share your results should you feel inspired to adapt, enhance, and optimize _GraphProto_ to your own very specific needs.

### Static Image

`Prototype` trait defines a high-level interface of a very generic widget. Any widget of this kind may be included in a list of objects displayed in a UI that is setup via `widgets` value of a main `Application` object.

    trait Prototype {

      protected val dimension: Dimension

      protected val numFrames = 256

      protected val frames: Seq[Frame]

      def frame(frame: Int) = frames(frame % numFrames)
    }

Extending `Prototype` trait requires you not only to define a physical `Dimension` of your image, but also a `numFrames` number of animation frames. There are two possible solutions to this complication. Either reassign `numFrames` value in a `Prototype` trait to be simply `1` and then initialize a sequence of frames with a sequence of just one `Frame` object, or keep `numFrames` value unchanged however populate all `frames` with a reference to one and the same `Frame` value.

So what is a `Frame`? It is a snapshot of your image taken at a very specific point of time. When rendering a static image there will always be just one item. When rendering a dynamic image, what is described in more details in the next chapter, you will surely want to setup a few more instances of a `Frame` class in order to generate a visually pleasant animation effect.

Any `Frame`-derived class is expected to conform to a very simple interface, namely it has to consist of just one publicly available method returning an instance of a `Canvas`-derived class.

    trait Frame {

      val canvas: Canvas
    }

Any instance of a `Canvas`-derived class needs to define a background colour and an instance of an `ImagePlus` class. This image constitutes a picture that is going to be rendered on a screen when repainting an application window.

    trait Canvas {

      protected val backgroundColor: Color

      val imagePlus: ImagePlus
    }

Once you setup your own classes deriving from GraphProto's generic interfaces, you will be able to simply plug an instance of your `Prototype` class into a list of objects that are eventually displayed by an executable program (see a list of `widgets` in an `Application` object and add your item there). If this all still sounds like a magic to you, check out the following _Tutorials_ section which provides a few hands-on examples with a source code that you can take and adapt according to your very own needs.

### Animated Frames

Not surprisingly rendering animated frames employs the same data structures as described in a previous paragraph on static images. You may simply apply the same rules just without any aforementioned limitations. There is one additional interface that takes care of setting up a list of frames for you. By extending `Model` trait the only method you need to include in your implementation is called `algorithm`.

    trait Model extends Prototype {

      protected def algorithm(frame: Int): Frame

      val frames = (0 until numFrames).map(algorithm(_))
    }

Given a single argument of a requested `frame` index (remember that you still have an access to `numFrames` value that is defined in a `Prototype` superclass), an instance of a `Frame`-derived class is expected to be returned. The remaining instructions from a previous paragraph hold true. Now the only difference is that you will actually setup a different image canvas for each and every frame of your animation. There are 3 example programs that this source code is bundled with _(Midpoint Circle,_ _Rotating Cube,_ and _Satan Cross)._ With a detailed _Hello World_ tutorial treating GraphProto as an animation rendering engine, you may follow their implementations to figure out any further details and to solve any unanswered questions that may still arise after reading all of the available documentation. If you still have any questions afterwards, do not hesitate to contact me directly, and I will be glad to help you out with resolving whatever issues you might possibly encounter.

Tutorials
---------

There are two very detailed step-by-step tutorials provided by an author, which are intended to help with a better understanding on how to use the library. The first one describes a minimal [Hello World](https://bitbucket.org/pawelkrol/graphproto/src/master/doc/HELLO.md)-like application, while the second one is a little bit more complex [Rotating Cube](https://bitbucket.org/pawelkrol/graphproto/src/master/doc/CUBE.md) example program.

Copyright & Licence
-------------------

Copyright (C) 2017-2024 by Pawel Krol.

This library is free open source software; you can redistribute it and/or modify it under [the same terms](https://bitbucket.org/pawelkrol/graphproto/src/master/LICENSE.md) as Scala itself, either Scala version 3.5.1 or, at your option, any later version of Scala you may have available.

PLEASE NOTE THAT IT COMES WITHOUT A WARRANTY OF ANY KIND!
