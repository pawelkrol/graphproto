object ProguardConf {

  val graphProto =
"""
-addconfigurationdebugging
-dontnote
-dontskipnonpubliclibraryclasses
-dontwarn
-keepattributes Signature
-optimizations "code/allocation/*,code/merging,code/removal/*,code/simplification/*,class/marking/*,class/merging/*,class/unboxing/*,field/*,method/inlining/*,method/marking/*,method/propagation/*,method/removal/*"

-keepclassmembers class * {
  ** MODULE$;
}

-keepclassmembers class org.bitbucket.pawelkrol.GraphProto.Algorithm.Point {
  *;
}

-keepclassmembers class org.bitbucket.pawelkrol.GraphProto.Application$ {
  *;
}

-keepclassmembers class org.bitbucket.pawelkrol.GraphProto.Widget.TrapezeFill {
  *;
}

-keep public class org.json4s.native.JsonParser$Parser {
  *;
}

-keep public class scala.swing.AbstractButton {
  *;
}

-keep public class scala.swing.Action {
  *;
}

-keep public class scala.swing.BoxPanel {
  *;
}

-keep public class scala.swing.Button {
  *;
}

-keep public class scala.swing.Dialog {
  *;
}

-keep public class scala.swing.FlowPanel {
  *;
}

-keep public class scala.swing.Label {
  *;
}

-keep public class scala.swing.MainFrame {
  *;
}

-keep public class scala.swing.Menu {
  *;
}

-keep public class scala.swing.MenuBar {
  *;
}

-keep public class scala.swing.MenuItem {
  *;
}

-keep public class scala.swing.Panel {
  *;
}

-keep public class scala.swing.RadioMenuItem {
  *;
}

-keep public class scala.swing.ScrollPane {
  *;
}
"""
}
