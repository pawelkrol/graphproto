lazy val root = (project in file(".")).settings(
  javaOptions += "-Xmx1G",
  maxErrors := 1,
  name := "graphproto",
  scalacOptions ++= Seq(
    "-deprecation",
    "-encoding", "UTF-8",
    "-explain",
    "-feature",
    "-language:implicitConversions",
    "-unchecked",
    "-Xfatal-warnings",
  ),
  scalaVersion := "3.5.1",
  version := "0.03-SNAPSHOT",
)

run / fork := true

libraryDependencies ++= Seq(
  "com.github.pawelkrol" % "config-manager" % "1.0.0-SNAPSHOT",
  "gov.nih.imagej" % "imagej" % "1.47",
  "org.scala-lang.modules" %% "scala-swing" % "3.0.0",
  "org.scalatest" %% "scalatest" % "3.2.19" % "test",
)

artifactName := { (sv: ScalaVersion, module: ModuleID, artifact: Artifact) =>
  artifact.name + "-" + module.revision + "." + artifact.extension
}

enablePlugins(SbtProguard)

Proguard / proguardFilteredInputs ++= ProguardOptions.noFilter((Compile / packageBin).value)

Proguard / proguardInputs := (Compile / dependencyClasspath).value.files

Proguard / proguardOptions += ProguardOptions.keepMain("org.bitbucket.pawelkrol.GraphProto.Application")

Proguard / proguardOptions += ProguardConf.graphProto

Proguard / proguardVersion := "7.6.0"
