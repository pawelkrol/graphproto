package org.bitbucket.pawelkrol.GraphProto

trait Dimension {

  val width: Int

  val height: Int
}
