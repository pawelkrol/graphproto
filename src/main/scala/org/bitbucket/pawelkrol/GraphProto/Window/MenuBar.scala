package org.bitbucket.pawelkrol.GraphProto
package Window

import java.awt.Font
import java.awt.Font.SANS_SERIF

import javax.swing.KeyStroke
import javax.swing.KeyStroke.getKeyStroke

import swing.{ Action, ButtonGroup, Menu, MenuItem, Publisher, RadioMenuItem }

class MenuBar(menu: List[Tuple2[String, List[Tuple3[String, String, () => Unit]]]]) extends swing.MenuBar {

  private val widgets = Application.widgets

  private val buttons = {
    val selectedWidget = Application.settings_("selected_object").asText
    val isValidWidget = widgets.find(widget => widget._1 == selectedWidget) match {
      case Some(_) => true
      case None => false
    }

    val widgetList = if (isValidWidget) {
      widgets.zipWithIndex.map({ case (button, index) => ((button, index), button._1 == selectedWidget) })
    }
    else {
      widgets.zipWithIndex.zipAll(List(true), null, false)
    }

    widgetList.map({ case (((label, widget), index), checked) =>
      val keyStroke = if (index < 9) Some(getKeyStroke("ctrl " + (index + 1))) else None
      buildRadioMenuItem(label, keyStroke, checked)
    })
  }

  private val buttonGroup = new ButtonGroup(buttons*)

  font = Font.getFont(SANS_SERIF)

  menu.foreach({ case (label, items) => {
    contents += new Menu(label) {
      label match {
        case "Objects" => {
          buttons.foreach(button => contents += button)
        }
        case _ =>
          items.foreach({ case (label, keyStroke, action) => {
            contents += new MenuItem(
              new Action(label) {
                def apply(): Unit = action()
                accelerator = Some(getKeyStroke(keyStroke))
              }
            )
          } })
      }
    }
  } })

  private def buildRadioMenuAction(title: String, keyStroke: Option[KeyStroke]) =
    new Action(title) {
      accelerator = keyStroke
      def apply(): Unit = {
        val actionName: String = peer.getValue(javax.swing.Action.NAME).toString
        val nowSelectedWidget = findWidgetByName(actionName)
        if (lastSelectedWidget != nowSelectedWidget) {
          lastSelectedWidget = nowSelectedWidget
          MenuBar.switchWidget
          Application.settings_.setValue("selected_object", actionName)
        }
      }
    }

  private def buildRadioMenuItem(title: String, keyStroke: Option[KeyStroke], checked: Boolean): RadioMenuItem =
    new RadioMenuItem("") {
      action = buildRadioMenuAction(title, keyStroke)
      selected = checked
    }

  def selectedWidget = widgets.find(widget => widget._1 == buttonGroup.selected.get.text).get._2

  private var lastSelectedWidget = selectedWidget

  private def findWidgetByName(name: String) = widgets.find(widget => widget._1 == name).get._2
}

object MenuBar extends Publisher {

  private val menu = List(
    ("File", List(
      ("Exit", "ctrl Q", () => publish(CloseProgram))
    )),
    ("Objects", List()),
    ("Help", List(
      ("About", "ctrl H", () => About().open())
    ))
  )

  def switchWidget: Unit = {
    publish(SwitchWidget)
  }

  def apply() = new MenuBar(menu)
}
