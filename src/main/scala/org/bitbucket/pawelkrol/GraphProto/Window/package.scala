package org.bitbucket.pawelkrol.GraphProto

import swing.event.Event

package object Window {

  case object CloseProgram extends Event

  case object SwitchWidget extends Event
}
