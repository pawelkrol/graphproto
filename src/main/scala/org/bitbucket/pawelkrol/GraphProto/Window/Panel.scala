package org.bitbucket.pawelkrol.GraphProto
package Window

import java.awt.{ Graphics2D, Rectangle }

import javax.swing.Timer

import swing.event.Key.Space
import swing.event.KeyPressed
import swing.Swing.ActionListener

class Panel extends swing.Panel {

  private val renderer = Renderer()

  focusable = true

  preferredSize = renderer.size

  listenTo(keys, MenuBar)

  reactions += {
    case event: KeyPressed =>
      event.key match {
        case Space =>
          animateNextFrame
        case _ =>
      }
    case SwitchWidget =>
      repaint()
  }

  override def paintComponent(g: Graphics2D): Unit = {
    val widget = Application.mainFrame.menu.selectedWidget
    val frame = widget.frame(numFrame)
    renderer.render(this, g, frame.canvas)
  }

  val animationInterval = 20 // in milliseconds

  val animationTimer = new Timer(animationInterval, ActionListener(e => animateNextFrame))

  animationTimer.start

  private def animateNextFrame: Unit = {
    numFrame += 1
    repaint()
  }

  private var numFrame = 0
}

object Panel {

  def apply() = new Panel
}
