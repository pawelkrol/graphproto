package org.bitbucket.pawelkrol.GraphProto
package Window

import swing.{ BoxPanel, Component, FlowPanel, Label, ScrollPane }
import swing.Orientation.{ Horizontal, Vertical }
import swing.Swing.EmptyBorder

class About extends Dialog {

  title = "About"

  private val infoText = "<html><p>GraphProto is a prototyping library designed to provide easy<br/>means of " +
    "miscellaneous graphic algorithm implementations.<br/><br/></p><center>Copyright (C) 2017-2024 Paweł Król</center></html>"

  val textArea: Component = new Label {
    enabled = true
    focusable = true
    opaque = false
    text = infoText
  }

  contents = new BoxPanel(Vertical) {

    border = EmptyBorder(weight = 10)

    contents += new FlowPanel(FlowPanel.Alignment.Left)(
      new BoxPanel(Vertical) {
        contents += new ScrollPane(textArea) {
          border = EmptyBorder(top = 0, left = 0, bottom = 10, right = 0)
        }
      }
    )

    contents += new BoxPanel(Horizontal) {
      border = EmptyBorder(weight = 5)
      contents += closeButton
    }
  }
}

object About {

  def apply() = new About
}
