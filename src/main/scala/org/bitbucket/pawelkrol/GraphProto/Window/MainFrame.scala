package org.bitbucket.pawelkrol.GraphProto
package Window

import javax.imageio.ImageIO

class MainFrame extends swing.MainFrame {

  val menu = MenuBar()

  iconImage = ImageIO.read(getClass.getResourceAsStream("/graphproto.png"))

  menuBar = menu

  listenTo(MenuBar)

  reactions += {
    case CloseProgram =>
      closeOperation()
  }

  contents = Panel()

  resizable = false

  title = "GraphProto 0.03-SNAPSHOT"

  // Place the window in the center of the screen
  peer.setLocationRelativeTo(null)

  override def closeOperation(): Unit = {
    Application.settings_.save
    super.closeOperation()
  }
}

object MainFrame {

  def apply() = new MainFrame
}
