package org.bitbucket.pawelkrol.GraphProto
package Window

import java.awt.Font.{ SANS_SERIF, getFont }
import java.awt.event.{ ActionEvent, ActionListener, KeyEvent }

import javax.swing.{ JComponent, KeyStroke }

import swing.Button
import swing.event.ButtonClicked

class Dialog extends swing.Dialog {

  font = getFont(SANS_SERIF)

  modal = true

  resizable = false

  val closeButton = new Button("Close")

  listenTo(closeButton)

  reactions += {
    case ButtonClicked(closeButton) =>
      dispose()
  }

  // Enable dialog close by pressing ESCAPE key
  peer.getRootPane.registerKeyboardAction(
    new ActionListener {
      override def actionPerformed(e: ActionEvent) = dispose()
    },
    KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE.toChar),
    JComponent.WHEN_IN_FOCUSED_WINDOW
  )

  // Place the window in the center of the screen
  peer.setLocationRelativeTo(null)
}
