package org.bitbucket.pawelkrol.GraphProto

import ij.process.ColorProcessor

import java.awt.Graphics2D

import Window.Panel

class Renderer(width: Int, height: Int) {

  def render(panel: Panel, g: Graphics2D, canvas: Canvas): Unit = {
    val ip = new ColorProcessor(canvas.imagePlus.getImage)
    g.drawImage(ip.resize(width, height).createImage, 0, 0, panel.peer)
  }

  val size = new java.awt.Dimension(width, height)
}

object Renderer {

  val defaultWidth = 320 * 3

  val defaultHeight = 200 * 3

  def apply() = new Renderer(defaultWidth, defaultHeight)
}
