package org.bitbucket.pawelkrol.GraphProto

class TextScreen extends Dimension {

  val width = 40

  val height = 25
}

object TextScreen {

  def apply() = new TextScreen
}
