package org.bitbucket.pawelkrol.GraphProto

import ij.ImagePlus

import java.awt.Image

import javax.swing.UIManager

class AWTCanvas(image: Image) extends Canvas {

  protected val backgroundColor = UIManager.getColor("Panel.background")

  val imagePlus = new ImagePlus("", image)
}

object AWTCanvas {

  def apply(image: Image) = new AWTCanvas(image)
}
