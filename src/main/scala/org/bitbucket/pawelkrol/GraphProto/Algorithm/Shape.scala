package org.bitbucket.pawelkrol.GraphProto
package Algorithm

case class Shape(polygons: Seq[Polygon]) {

  def visiblePolygons(observer: Vertex) = polygons.filter(_.isVisible(observer))

  def scale(vector: Vertex) = Shape(polygons.map(_.scale(vector)))

  def translate(vector: Vertex) = Shape(polygons.map(_.translate(vector)))

  def rotate(vector: Vertex) = Shape(polygons.map(_.rotate(vector)))
}
