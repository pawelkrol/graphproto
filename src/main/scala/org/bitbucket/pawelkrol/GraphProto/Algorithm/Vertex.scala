package org.bitbucket.pawelkrol.GraphProto
package Algorithm

import java.lang.Math.{ cos, sin }

case class Vertex(x: Double, y: Double, z: Double) {

  def translate(point: Vertex) = Vertex(x + point.x, y + point.y, z + point.z)

  def -(that: Vertex) = Vertex(x - that.x, y - that.y, z - that.z)

  def unary_- = Vertex(-x, -y, -z)

  def *(that: Vertex) = x * that.x + y * that.y + z * that.z

  lazy val mirrored = Vertex(-x, -y, -z)

  def scale(vector: Vertex) = Vertex(x * vector.x, y * vector.y, z * vector.z)

  def perspectiveCast(distance: Double) = Vertex(distance * x / z, distance * y / z, 0)

  private val vertexMatrix = List(x, y, z, 1)

  def rotate(vector: Vertex) = {
    val cos_x = cos(vector.x)
    val cos_y = cos(vector.y)
    val cos_z = cos(vector.z)
    val sin_x = sin(vector.x)
    val sin_y = sin(vector.y)
    val sin_z = sin(vector.z)

    val rotationMatrix: List[List[Double]] = List(
      List(cos_y * cos_z, -sin_z, -sin_y, 0),
      List(sin_z, cos_x * cos_z, sin_x, 0),
      List(sin_y, -sin_x, cos_x * cos_y, 0),
      List(0, 0, 0, 1)
    )

    // vertexMatrix * rotationMatrix
    val product = (0 until 4).map(column => rotationMatrix.map(_(column))).map(rotation => {
      vertexMatrix.zip(rotation).foldLeft[Double](0)((sum, factors) => sum + factors._1 * factors._2)
    })

    Vertex(product.take(3))
  }
}

object Vertex {

  def apply(x: Double) = new Vertex(x, x, x)

  def apply(x: Seq[Double]) = new Vertex(x(0), x(1), x(2))

  def apply(point: Tuple3[Double, Double, Double]) = new Vertex(point._1, point._2, point._3)
}
