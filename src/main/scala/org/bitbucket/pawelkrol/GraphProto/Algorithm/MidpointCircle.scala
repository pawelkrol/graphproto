package org.bitbucket.pawelkrol.GraphProto
package Algorithm

class MidpointCircle(x: Int, y: Int, r: Int) {

  val center = Point(x, y)

  val points: Set[Point] = calculatePoints

  private def calculatePoints = {

    // Obtain the first point on a circle centered on the origin
    val pp = List[Point](Point(0, r))

    // Calculate the initial value of the decision parameter
    val p0 = 5 / 4 - r

    // At each xk position, starting at k = 0, perform next point calculations
    val octant = region(p0, pp)

    // Determine symmetry points in the other seven octants
    val points = octant.flatMap(_.octantSymmetryPoints)

    // Move each calculated pixel position (x, y) onto the circular path centred on (x0, y0)
    points.map(_.translate(center)).toSet
  }

  private def region(pk: Double, pp: List[Point]): List[Point] = {

    val x = pp.head.x + 1

    // If pk < 0, the next point along the circle centered on (0, 0) is (x+1,y)
    val y = if (pk < 0)
      pp.head.y
    // Otherwise, the next point along the ellipse centered on (0, 0) is (x+1, y-1)
    else
      pp.head.y - 1

    val pknext = if (pk < 0)
      pk + 2 * x + 1
    else
      pk + 2 * x + 1 - 2 * y

    val ppnext: List[Point] = Point(x, y) :: pp

    if (x <= y)
      region(pknext, ppnext)
    else
      ppnext
  }
}

object MidpointCircle {

  def apply(x: Int, y: Int, r: Int) = new MidpointCircle(x, y, r)
}
