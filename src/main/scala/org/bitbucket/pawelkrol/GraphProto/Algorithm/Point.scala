package org.bitbucket.pawelkrol.GraphProto
package Algorithm

import java.lang.Math.{ cos, sin }

case class Point(x: Int, y: Int) {

  def octantSymmetryPoints =
    Set((-x, -y), (-x, y), (x, -y), (x, y), (-y, -x), (-y, x), (y, -x), (y, x)).map(point => Point(point._1, point._2))

  def translate(point: Point) = Point(x + point.x, y + point.y)

  def rotate(angle: Double) = {
    val xRotated = x * cos(angle) - y * sin(angle)
    val yRotated = y * cos(angle) + x * sin(angle)

    Point(xRotated.toInt, yRotated.toInt)
  }

  def rotate(angle: Double, center: Point): Point = translate(center.mirrored).rotate(angle).translate(center)

  lazy val mirrored = Point(-x, -y)
}

object Point {

  def apply(point: Tuple2[Int, Int]) = new Point(point._1, point._2)
}
