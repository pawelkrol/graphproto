package org.bitbucket.pawelkrol.GraphProto
package Algorithm

import java.lang.Math.abs

class BresenhamLine(point1: Point, point2: Point) {

  val points: Set[Point] = calculatePoints

  private def calculatePoints = {

    // Calculate constants dx, and dy
    val dx = abs(point2.x - point1.x)
    val dy = abs(point2.y - point1.y)

    // Determine which point to use as start, which as end
    val (x, y, yn) =
      if (point1.x < point2.x)
        (point1.x, point1.y, point2.y)
      else
        (point2.x, point2.y, point1.y)

    // Check whether line drawing happens towards top or bottom
    val direction = if (y < yn) 1 else -1

    // Plot the first point
    val points = Set(Point(x, y))

    // Line drawing for slopes in the range 0 < |m| < 1
    if (dx > dy) {

      // Calculate constants two_dy, and two_dy_minus_two_dx
      val two_dy = 2 * dy
      val two_dy_minus_two_dx = two_dy - 2 * dx

      // Obtain the starting value for the decision parameter
      val p = two_dy - dx

      def computePoints(k: Int, p: Int, x: Int, y: Int): Set[Point] = {
        // We step along the x direction in unit steps and calculate successive y values nearest the line path
        if (k > dx)
          points
        else {
          val xNext = x + 1

          val (pNext, yNext) =
            // If pk < 0, the next point to plot is (xk + 1, yk)
            if (p < 0)
              (p + two_dy, y)
            // Otherwise, the next point to plot is (xk + 1, yk + 1)
            else
              (p + two_dy_minus_two_dx, y + direction)

          computePoints(k + 1, pNext, xNext, yNext) + Point(xNext, yNext)
        }
      }

      // At each xk along the line, starting at k = 0, perform the test
      computePoints(1, p, x, y)
    }

    // Line drawing for slopes in the range |m| > 1
    else {

      // Calculate constants two_dx, and two_dx_minus_two_dy
      val two_dx = 2 * dx
      val two_dx_minus_two_dy = two_dx - 2 * dy

      // Obtain the starting value for the decision parameter
      val p = two_dx - dy

      def computePoints(k: Int, p: Int, x: Int, y: Int): Set[Point] = {
        // We step along the y direction in unit steps and calculate successive x values nearest the line path
        if (k > dy)
          points
        else {
          val yNext = y + direction

          val (pNext, xNext) =
            // If pk < 0, the next point to plot is (xk + 1, yk)
            if (p < 0)
              (p + two_dx, x)
            // Otherwise, the next point to plot is (xk + 1, yk + 1)
            else
              (p + two_dx_minus_two_dy, x + 1)

          computePoints(k + 1, pNext, xNext, yNext) + Point(xNext, yNext)
        }
      }

      // At each yk along the line, starting at k = 0, perform the test
      computePoints(1, p, x, y)
    }
  }
}

object BresenhamLine {

  def apply(x1: Int, y1: Int, x2: Int, y2: Int) = new BresenhamLine(Point(x1, y1), Point(x2, y2))

  def apply(point1: Point, point2: Point) = new BresenhamLine(point1, point2)
}
