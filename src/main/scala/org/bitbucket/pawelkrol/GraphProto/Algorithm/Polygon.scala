package org.bitbucket.pawelkrol.GraphProto
package Algorithm

import java.awt.{ BasicStroke, Color, Graphics2D }

case class Polygon(vertices: Seq[Vertex], color: Option[Color]) {

  private def cross(a: Vertex, b: Vertex) = Vertex(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x)

  private def normal = cross(vertices(2) - vertices(1), vertices(1) - vertices(0))

  private val farthestVertex = vertices.sortWith(_.z > _.z).head

  def isVisible(observer: Vertex) = {
    val perspectiveDistance = -0.8 * observer.z / farthestVertex.z
    val perspectiveCasted = Vertex(farthestVertex.x * perspectiveDistance, farthestVertex.y * perspectiveDistance, 0)
    (farthestVertex.translate(perspectiveCasted) - observer) * normal >= 0
  }

  def scale(vector: Vertex) = Polygon(vertices.map(_.scale(vector)), color)

  def translate(vector: Vertex) = Polygon(vertices.map(_.translate(vector)), color)

  def perspectiveCast(distance: Double) = Polygon(vertices.map(_.perspectiveCast(distance)), color)

  private val lines = vertices.tail.zip(vertices.init) :+ (vertices.head, vertices.last)

  def draw(g2d: Graphics2D): Unit = {
    val strokeColor = g2d.getColor
    color match {
      case Some(c) => {
        g2d.setColor(c)
        g2d.fillPolygon(vertices.map(_.x.toInt).toArray, vertices.map(_.y.toInt).toArray, vertices.size)
      }
      case None =>
    }
    g2d.setStroke(new BasicStroke(2))
    g2d.setColor(strokeColor)
    lines.foreach({ case (u, v) =>
      g2d.drawLine(u.x.toInt, u.y.toInt, v.x.toInt, v.y.toInt)
    })
  }

  def rotate(vector: Vertex) = Polygon(vertices.map(_.rotate(vector)), color)
}
