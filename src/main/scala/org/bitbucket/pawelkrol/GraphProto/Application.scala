package org.bitbucket.pawelkrol.GraphProto

import com.github.pawelkrol.ConfigManager.Settings.load
import com.github.pawelkrol.ConfigManager.Types.Hash

import swing.SimpleSwingApplication

import Widget.{ Circle, RotatingCube, SatanCross }
import Window.MainFrame

object Application extends SimpleSwingApplication {

  val widgets = List(
    ("Midpoint Circle", Circle()),
    ("Satan Cross", SatanCross()),
    ("Rotating Cube", RotatingCube())
  )

  val mainFrame = MainFrame()

  private def defaultConfiguration = Hash(
    "selected_object" -> Application.widgets(0)._1,
  )

  lazy val settings_ = load(
    fileName = ".graphproto.json",
    defaults = defaultConfiguration,
  )

  def top = mainFrame
}
