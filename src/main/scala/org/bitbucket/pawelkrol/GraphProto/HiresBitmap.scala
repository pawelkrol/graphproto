package org.bitbucket.pawelkrol.GraphProto

class HiresBitmap extends Dimension {

  val width = 320

  val height = 200
}

object HiresBitmap {

  def apply() = new HiresBitmap
}
