package org.bitbucket.pawelkrol.GraphProto
package Widget

import Algorithm.{ BresenhamLine, Point }

class SatanCrossFrame(rotationAngle: Double, width: Int, height: Int) extends Frame {

  private val background = 0x0f

  private val color = 0x0b

  private val xCenter = width / 2

  private val yCenter = 7

  private val longerArm = {
    val x1 = width / 2
    val y1 = 2
    val x2 = x1
    val y2 = height - 3

    rotatedArm(x1, y1, x2, y2)
  }

  private val shorterArm = {
    val x1 = width / 2 - width / 5 + 1
    val y1 = height - height / 3 - 1
    val x2 = width / 2 + width / 5 - 1
    val y2 = y1

    rotatedArm(x1, y1, x2, y2)
  }

  private def rotatedArm(x1: Int, y1: Int, x2: Int, y2: Int) =
    BresenhamLine(rotatedPoint(x1, y1), rotatedPoint(x2, y2)).points

  private def rotatedPoint(x: Int, y: Int) = {

    val rotationCenter = Point(xCenter, yCenter)

    Point(x, y).rotate(rotationAngle, rotationCenter)
  }

  private val points =
    (longerArm ++ shorterArm).map(enlargedPoint(_)).flatten.toSet.toList.map((point: Point) => (point.x, point.y, color))

  private def enlargedPoint(point: Point) = {
    val x = point.x
    val y = point.y

    List(
      (x, y),
      (x - 1, y),
      (x + 1, y),
      (x, y - 1),
      (x, y + 1)
    ).map(Point(_))
  }

  // private val spike = (xCenter, yCenter, 0x0c)

  lazy val canvas = C64Canvas(background, points, width, height)
}

object SatanCrossFrame {

  def apply(rotationAngle: Double, width: Int, height: Int) = new SatanCrossFrame(rotationAngle, width, height)
}
