package org.bitbucket.pawelkrol.GraphProto
package Widget

import Algorithm.MidpointCircle

class CircleFrame(x: Int, y: Int, r: Int, width: Int, height: Int) extends Frame {

  private val background = 0x05

  private val color = 0x00

  private val points = MidpointCircle(x, y, r).points.toList.map(point => (point.x, point.y, color))

  lazy val canvas = C64Canvas(background, points, width, height)
}

object CircleFrame {

  def apply(x: Int, y: Int, r: Int, width: Int, height: Int) = new CircleFrame(x, y, r, width, height)
}
