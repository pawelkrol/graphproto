package org.bitbucket.pawelkrol.GraphProto
package Widget

import java.lang.Math.{ PI, sin }

class SatanCross extends Model {

  lazy val dimension = TextScreen()

  protected def algorithm(frame: Int) = {
    val rotationRange = PI / 6
    val rotationAngle = rotationRange * sin(4 * 2 * PI * frame / numFrames)

    SatanCrossFrame(rotationAngle, dimension.width, dimension.height)
  }
}

object SatanCross {

  def apply() = new SatanCross
}
