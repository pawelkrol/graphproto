package org.bitbucket.pawelkrol.GraphProto
package Widget

import java.lang.Math.PI

class RotatingCube extends Model {

  lazy val dimension = HiresBitmap()

  protected def algorithm(frame: Int) = {
    val angleX = 4 * PI * frame / numFrames
    val angleY = 2 * PI * frame / numFrames
    val angleZ = -2 * PI * frame / numFrames

    RotatingCubeFrame(dimension.width, dimension.height, angleX, angleY, angleZ)
  }
}

object RotatingCube {

  def apply() = new RotatingCube
}
