package org.bitbucket.pawelkrol.GraphProto
package Widget

import java.lang.Math.{ PI, sin }

class Circle extends Model {

  lazy val dimension = HiresBitmap()

  private def sinus(frame: Int, shift: Double, scale: Int, multiply: Int) =
    (scale * sin(4 * PI * multiply * frame / numFrames + shift)).toInt

  protected def algorithm(frame: Int) = {
    val width = dimension.width
    val height = dimension.height

    // Compute sine with a given shift
    val sinusX = sinus(frame, 0, width / 3, 1)
    val sinusY = sinus(frame, 0, height / 8, 2)
    val smallerSide = width.min(height)
    val sinusR = sinus(frame, PI / 2, smallerSide / 10, 2)

    // Compute center X
    val x = width / 2 + sinusX

    // Compute center Y
    val y = height / 2 + sinusY

    // Compute radius R
    val r = smallerSide / 4 + sinusR

    CircleFrame(x, y, r, width, height)
  }
}

object Circle {

  def apply() = new Circle
}
