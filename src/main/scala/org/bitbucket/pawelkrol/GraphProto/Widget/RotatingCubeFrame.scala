package org.bitbucket.pawelkrol.GraphProto
package Widget

import java.awt.{ Color, Graphics2D }
import java.awt.image.BufferedImage
import java.awt.image.BufferedImage.TYPE_INT_RGB

import java.lang.Math.{ cos, sin }

import Algorithm.{ Polygon, Shape, Vertex }

class RotatingCubeFrame(width: Int, height: Int, angleX: Double, angleY: Double, angleZ: Double) extends Frame {

  private val background = new Color(0xee, 0xe8, 0xd5)

  private val color = new Color(0x07, 0x36, 0x42)

  private val observer = Vertex(0, 0, -1024)

  private val distance = height.toDouble / 0.75

  private val center = Vertex(width / 2, height / 2, 0)

  private val rotatedShape = RotatingCubeFrame.shape.rotate(Vertex(angleX, 0, 0)).rotate(Vertex(0, angleY, 0)).rotate(Vertex(0, 0, angleZ)).translate(-observer)

  private val visiblePolygons = rotatedShape.visiblePolygons(observer)

  private val castedPolygons = visiblePolygons.map(_.perspectiveCast(distance))

  private val translatedPolygons = castedPolygons.map(_.translate(center))

  private def drawPolygons(g2d: Graphics2D): Unit = {
    translatedPolygons.map(_.draw(g2d))
  }

  private val cube = {

    val img = new BufferedImage(width, height, TYPE_INT_RGB)

    val g2d = img.createGraphics
    g2d.setColor(background)
    g2d.fillRect(0, 0, width, height)
    g2d.setColor(color)
    drawPolygons(g2d)
    g2d.dispose

    img
  }

  lazy val canvas = AWTCanvas(cube)
}

object RotatingCubeFrame {

  private val vertices = List(
    (-200.0, 200.0, -200.0),
    (200.0, 200.0, -200.0),
    (200.0, -200.0, -200.0),
    (-200.0, -200.0, -200.0),
    (-200.0, 200.0, 200.0),
    (200.0, 200.0, 200.0),
    (200.0, -200.0, 200.0),
    (-200.0, -200.0, 200.0)
  ).map(Vertex(_))

  private val colors = Seq(
    new Color(0xb5, 0x89, 0x00),
    new Color(0xcb, 0x4b, 0x16),
    new Color(0x6c, 0x71, 0xc4),
    new Color(0x26, 0x8b, 0xd2),
    new Color(0x2a, 0xa1, 0x98),
    new Color(0x85, 0x99, 0x00)
  )

  private val shape = Shape(Seq(
    Seq(vertices(0), vertices(1), vertices(2), vertices(3)),
    Seq(vertices(4), vertices(7), vertices(6), vertices(5)),
    Seq(vertices(4), vertices(0), vertices(3), vertices(7)),
    Seq(vertices(1), vertices(5), vertices(6), vertices(2)),
    Seq(vertices(0), vertices(4), vertices(5), vertices(1)),
    Seq(vertices(3), vertices(2), vertices(6), vertices(7))
  ).zip(colors).map({ case (vertices, color) => Polygon(vertices, Some(color)) }))

  def apply(width: Int, height: Int, angleX: Double, angleY: Double, angleZ: Double) =
    new RotatingCubeFrame(width, height, angleX, angleY, angleZ)
}
