package org.bitbucket.pawelkrol.GraphProto

import ij.ImagePlus
import ij.process.ColorProcessor

import java.awt.Color

import javax.swing.UIManager

class RGBCanvas(background: Color, points: List[Tuple3[Int, Int, Color]]) extends Canvas {

  protected val backgroundColor = UIManager.getColor("Panel.background")

  val imagePlus = {
    val width = Renderer.defaultWidth
    val height = Renderer.defaultHeight

    val ip = new ColorProcessor(width, height)

    ip.setColor(backgroundColor)
    ip.setRoi(0, 0, width, height)
    ip.fill

    points.foreach({ case (x, y, color) => {
      ip.setColor(color)
      ip.drawPixel(x, y)
    } })

    new ImagePlus("", ip)
  }
}

object RGBCanvas {

  def apply(background: Color, points: List[Tuple3[Int, Int, Color]]) = new RGBCanvas(background, points)
}
