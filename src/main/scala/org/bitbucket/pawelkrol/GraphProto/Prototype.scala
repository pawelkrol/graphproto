package org.bitbucket.pawelkrol.GraphProto

trait Prototype {

  protected lazy val dimension: Dimension

  protected val numFrames = 256

  protected val frames: Seq[Frame]

  def frame(frame: Int) = frames(frame % numFrames)
}
