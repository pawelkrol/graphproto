package org.bitbucket.pawelkrol.GraphProto

trait Model extends Prototype {

  protected def algorithm(frame: Int): Frame

  val frames = (0 until numFrames).map(algorithm(_))
}
