package org.bitbucket.pawelkrol.GraphProto

import ij.ImagePlus

import java.awt.Color

trait Canvas {

  protected val backgroundColor: Color

  val imagePlus: ImagePlus
}
