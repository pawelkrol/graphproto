package org.bitbucket.pawelkrol.GraphProto

class DefaultWindow extends Dimension {

  val width = Renderer.defaultWidth

  val height = Renderer.defaultHeight
}

object DefaultWindow {

  def apply() = new DefaultWindow
}
