package org.bitbucket.pawelkrol.GraphProto

import ij.ImagePlus
import ij.process.ColorProcessor

import java.awt.Color

class C64Canvas(
  background: Int,
  points: List[Tuple3[Int, Int, Int]],
  width: Int,
  height: Int
) extends Canvas {

  private val colodorePalette = List(
    (0x00, 0x00, 0x00), // black
    (0xff, 0xff, 0xff), // white
    (0x81, 0x33, 0x38), // red
    (0x75, 0xce, 0xc8), // cyan
    (0x8e, 0x3c, 0x97), // purple
    (0x56, 0xac, 0x4d), // green
    (0x2e, 0x2c, 0x9b), // blue
    (0xed, 0xf1, 0x71), // yellow
    (0x8e, 0x50, 0x29), // orange
    (0x55, 0x38, 0x00), // brown
    (0xc4, 0x6c, 0x71), // light red
    (0x4a, 0x4a, 0x4a), // dark grey
    (0x7b, 0x7b, 0x7b), // grey
    (0xa9, 0xff, 0x9f), // light green
    (0x70, 0x6d, 0xeb), // light blue
    (0xb2, 0xb2, 0xb2)  // light grey
  )

  private def paletteColor(color: Int) = {
    val rgb = colodorePalette(color)
    new Color(rgb._1, rgb._2, rgb._3)
  }

  protected val backgroundColor = paletteColor(background)

  val imagePlus = {
    val ip = new ColorProcessor(width, height)

    ip.setColor(backgroundColor)
    ip.setRoi(0, 0, width, height)
    ip.fill

    points.foreach({ case (x, y, color) => {
      ip.setColor(paletteColor(color))
      ip.drawPixel(x, y)
    } })

    new ImagePlus("", ip)
  }
}

object C64Canvas {

  def apply(background: Int, points: List[Tuple3[Int, Int, Int]], width: Int, height: Int) =
    new C64Canvas(background, points, width, height)
}
